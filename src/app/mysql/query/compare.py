#!/usr/bin/python
# coding=utf-8

from MySQLdb import cursors
from settings import *
import MySQLdb
import string

def connect(db='xml_dblp'):
  return MySQLdb.connect(
    host=SQL_HOST,
    port=SQL_PORT,
    user=SQL_USER,
    passwd=SQL_PASSWORD,
    db=db,
    use_unicode=True,
    charset=SQL_CHARSET
  )

def close(connection):
  return connection.close()

def compare(slca, keyword='quality analysis data', db='xml_dblp'):
  # Normalize keyword
  replace_punctuation = string.maketrans(
    PUNCTUATION, ' '*len(PUNCTUATION))
  keyword_norm = keyword.translate(replace_punctuation)
  words = keyword_norm.lower().split()

  # Connection and cursor
  connection = connect(db)
  cursor = connection.cursor()

  stmt = '''
    select count(*) count
    from word_inode
    where depth >= %s and word in (%s)
    order by inode_id
  '''
  in_p=', '.join(list(map(lambda x: '%s', words)))

  ############### NONE DEPTH  ###############
  # All instance posting lists with depth 0 store here
  sql0 = stmt % (0, in_p)
  cursor.execute(sql0, words)
  count0 = cursor.fetchall()[0][0]

  ############### WITH DEPTH ################
  # Calculate depth of each slca node
  depths = [len(s.split('.'))-1 for s in slca]
  # Hmm, minimum depth of all slca node
  t = min_depth(depths)
  sqlt = stmt % (t, in_p)
  cursor.execute(sqlt, words)
  countt = cursor.fetchall()[0][0]

  connection.close()
  return [count0, countt]


def min_depth(args):
  if not args:
    return 0
  elif len(args) == 1:
    return args[0]
  else:   # arg has more than two elements
    return min(*args)

if __name__ == '__main__':
  from keyword_list import dblp, nasa, sigmod, xmark_02, xmark_10
  from schema import get_posting_list
  from settings import *
  from slca import slca
  from pprint import pprint
  # from sys import argv

  # # Get keyword and database
  # if len(argv) == 1:
  #   keyword = KEYWORD
  #   db = SQL_DB
  # elif len(argv) == 2:
  #   keyword = argv[1]
  #   db = SQL_DB
  # else:
  #   keyword = argv[1]
  #   db = argv[2]

  for keyword in xmark_10[3]:
    # db = 'sigmod'
    # db = 'xmark_02'
    db = 'xmark_10'
    # db = 'xml_dblp'
    # db = 'xml_nasa'
    # Get SLCA list
    posting_lists = get_posting_list(keyword=keyword, db=db).values()
    # pprint(posting_lists)
    SLCA = slca(posting_lists)

    # Get results to compare
    results = compare(SLCA, keyword, db)
    print keyword
    print results[0]
    print results[1]
    print '=========='
