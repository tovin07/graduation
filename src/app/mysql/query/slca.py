#!/usr/bin/python
# coding=utf-8

from lca import *
from schema import get_posting_list
from settings import *

def get_slca(L1, L2):
  SLCA = []
  u = '0'
  for v in L1:
    # Calculate lca of v with it left, right match in L2
    vl = lca(v, lm(v, L2))
    vm = lca(v, rm(v, L2))
    # Get the descendant of the two lca
    x = descendant(vl, vm)
    # Get the snode_id of u and x
    u_id = int(u.split('.')[-1])
    x_id = int(x.split('.')[-1])
    # If u_id <= x_id, let's consider who is descentant
    if u_id <= x_id:
      if u not in x:    # If u is not descendant of x, append u to SLCA list
        SLCA.append(u)
      u = x             # Assign u = x
  SLCA.append(u)        # Append the remaining u to SLCA
  return SLCA           # Return SLCA

def slca(L):
  # If L has at least two sub-lists, let's rock
  if len(L) > 1:
    # If L is empty or there are at least one list in L has length equals to 0
    # Return empty list => Query has no result
    if min(*[len(Li) for Li in L]) == 0:
      return []
    else:
      # Calculate the first SLCA of the first two lists
      L0 = L[0]
      L1 = L[1]
      SLCA = get_slca(L0, L1)
      # Now, consider ther rest of L only, skip the first two list
      remain_L = L[2:]
      # Loop over remaining lists
      for Li in remain_L:
        SLCA = get_slca(SLCA, Li)
      # Return result
      return SLCA
  # Having only one list? No ploblem, just return that list
  elif len(L) == 1:
    return L[0]
  # Ohm, empty thing? Empty in will get empty out only :v
  elif not L:
    return []

if __name__ == '__main__':
  from keywords import *
  from pprint import pprint
  from schema import get_posting_list
  for keyword in dblp_2002.values():
    posting_lists = get_posting_list(keyword=keyword,db='xml_dblp').values()
    print posting_lists
    SLCA = slca(posting_lists)
    if len(SLCA) > 1:
      print keyword
      print SLCA
      print min(*[len(nlp.split('.'))-1 for nlp in SLCA])
      print '===='
