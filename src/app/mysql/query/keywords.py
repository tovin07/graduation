#!/usr/bin/python
# coding=utf-8

dblp_2002 = {
  'QD1': 'flexibility',
  'QD2': 'scheduling management',
  'QD3': 'quality analysis data',
  'QD4': 'rule programming object system',
  'QD5': 'Levy J Jagadish H',
  'QD6': 'flexibility message scheme',
  'QD7': 'ICDE XML Jagadish',
  'QD8': 'distributed database systems performance analysis Micheal Stonebraker John Woodfill'
}

nasa = {
  'QN1': 'astroObjects',
  'QN2': 'Micheal magnitude',
  'QN3': 'photometry galatic cluster Astron',
  'QN4': 'pleiades dataset',
  'QN5': 'PAZh components',
  'QN6': 'pleiades journal',
  'QN7': 'textFile name',
  'QN8': 'acurate positions of 502 stars Eichhorn Googe Murphy Lukac'
}

sigmod = {
  'QS1': 'database',
  'QS2': 'object management',
  'QS3': 'research articles',
  'QS4': 'micheal journal',
  'QS5': 'query position',
  'QS6': 'information of research paper number',
  'QS7': 'notes in database implementation',
  'QS8': 'processing performance language'
}

xmark = {
  'QX1': 'Zurich',
  'QX2': 'Arizona Mehrdad edu',
  'QX3': 'Takano sun com mailto',
  'QX4': 'homepage name',
  'QX5': 'helena 96',
  'QX6': 'mehrdad takano net',
  'QX7': 'person id person0 name',
  'QX8': 'harpreet mahony nodak edu 99 lazaro st el svalbard an jan mayen island'
}
