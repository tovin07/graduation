#!/usr/bin/python
# coding=utf-8

from query import i_posting_list
from pprint import pprint

def xpath_evaluation(slca, keyword='XML Levy'):
  # slca converted to list
  slca_list = [s.split('.') for s in slca]
  # Get list of snode_id
  snode_ids = [int(s[-1]) for s in slca_list]
  # Calculate depth of each slca node
  depths = [len(s)-1 for s in slca_list]
  # Hmm, minimum depth of all slca node
  t = min_depth(depths)
  # All instance posting lists store here
  L = i_posting_list(keyword=keyword, depth=t).values()
  # get first list
  L1 = L.pop(0)
  # this is result list, initialize it with key-only dictionary
  R = {}
  for count, s in enumerate(slca):
    R[slca[count]] = []

  for node in L1:
    # numeric label path in list style
    nmlpl = node['numeric_label_path'].split('.')
    # id of instance node in list style
    npl = node['node_path'].split('.')
    for count, s in enumerate(snode_ids):
      # Depth at ith level
      di = depths[count]
      # Skip nodes have depth lower than di
      try:
        # If snode id of node at depth ith is equal to s, play the game
        if int(nmlpl[di]) == s:
          flag = 1
          for Lj in L:          # First list is removed
            # Get match_node.  int(npl[di]) is inode id of node at depth di
            match_node = find_matching_posting_list(di, int(npl[di]), Lj)
            # If one match_node is False => No answer. Next immediately
            if not match_node:
              flag = 0  # Set flag to 0 and break this very expensive loop
              break
          # Check flag. If flag is still 1 => Good => One result found
          if flag:
            R[slca[count]].append(int(npl[di]))
      except:
        pass
  return R

def find_matching_posting_list(di, inode_id, Lj):
  # Get node_path generator contain tuples (inode_id, node_path)
  npl = get_node_path(Lj)
  # calculate rm(inode_id, Lj) | Hmm, rm stands right match
  rm_node = rm(inode_id, npl)
  try:
    # check if rm_node[di] == inode_id
    if int(rm_node[di]) == inode_id:
      return True
  except Exception, e:
    return False
  # Return rm if True else False

def get_node_path(L):
  for node in L:
    yield (node['inode_id'], node['node_path'].split('.'))

def rm(inode_id, L):
  # L is list of node_path that splitted in to list of string
  # this functions is only work with sorted list
  for id, np in L:
    if id >= inode_id:
      return np

def min_depth(args):
  if not args:
    return 0
  elif len(args) == 1:
    return args[0]
  else:   # arg has more than two elements
    return min(*args)

if __name__ == '__main__':
  from pprint import pprint
  from schema import get_posting_list
  from settings import *
  from slca import slca
  keyword = 'XML Levy'
  posting_lists = get_posting_list(keyword=keyword, db='xml_dblp').values()
  slca = slca(posting_lists)
  print xpath_evaluation(slca, keyword)
