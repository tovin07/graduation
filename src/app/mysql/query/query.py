#!/usr/bin/python
# coding=utf-8

from MySQLdb import cursors
from settings import *
from time import time
import MySQLdb
import string

def connect(db='xml_dblp'):
  return MySQLdb.connect(
    host=SQL_HOST,
    port=SQL_PORT,
    user=SQL_USER,
    passwd=SQL_PASSWORD,
    db=db,
    use_unicode=True,
    charset=SQL_CHARSET,
    cursorclass = cursors.DictCursor
    # Control large dataset
    # cursorclass = cursors.SSCursor
  )

def close(connection):
  return connection.close()

# Instance posting list to distinguish with schema posting list
def i_posting_list(keyword='XML Levy', depth=0):
  connection = connect()
  cursor = connection.cursor()
  # Normalize keyword
  replace_punctuation = string.maketrans(
    PUNCTUATION, ' '*len(PUNCTUATION))
  keyword_norm = keyword.translate(replace_punctuation)
  words = keyword_norm.lower().split()

  stmt = '''
    select inode_id, node_path, numeric_label_path
    from word_inode
    where word = %s and depth >= %s
    order by inode_id
  '''

  # Posting list (generator)
  L = {}

  for word in words:
    cursor.execute(stmt, (word, depth))
    L[word] = cursor.fetchall()

  # Remember to close connection
  connection.close()
  return L

if __name__ == '__main__':
  L = i_posting_list()
  # print L.keys()
  print L['xml'][0]
  print L['xml'][1]
