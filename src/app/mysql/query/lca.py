#!/usr/bin/python
# coding=utf-8

def lca(v, w):
  vnlp = v.split('.')
  wnlp = w.split('.')
  try:
    lca = vnlp[:[x[0]==x[1] for x in zip(vnlp,wnlp)].index(0)]
  except Exception, e:
    return '.'.join(vnlp)
  else:
    return '.'.join(lca)  # Return numeric_label_path

def lm(v, L):
  vsnode_id = int(v.split('.')[-1])
  tmp_L = dict([(int(n.split('.')[-1]), n) for n in L])
  holds = [n for n in tmp_L.keys() if n <= vsnode_id]
  if not holds:
    return '0'  # Return root node
  elif len(holds) == 1:
    return tmp_L[holds[0]]
  else:
    lmid = max(*holds)
    return tmp_L[lmid]

def rm(v, L):
  vsnode_id = int(v.split('.')[-1])
  tmp_L = dict([(int(n.split('.')[-1]), n) for n in L])
  holds = [n for n in tmp_L.keys() if n >= vsnode_id]
  if not holds:
    return '0'  # Return root node
  elif len(holds) == 1:
    return tmp_L[holds[0]]
  else:
    rmid = min(*holds)
    return tmp_L[rmid]

def descendant(vnlp1, vnlp2):
  if vnlp1 in vnlp2:
    return vnlp2
  elif vnlp2 in vnlp1:
    return vnlp1
  else:
    return '0'

def is_ancestor(vnlp1, vnlp2):
  # Check if vnlp1 is descendant of vnlp2
  if vnlp1 and vnlp1.split('.')[-1] in vnlp2:
    return True
  else:
    return False

def is_descendant(vnlp1, vnlp2):
  # Check if vnlp1 is descendant of vnlp2
  if vnlp2 and vnlp2.split('.')[-1] in vnlp1:
    return True
  else:
    return False

if __name__ == '__main__':
  from settings import *
  # v = {
  #   'numeric_label_path': '0.1.2.3.5.6'
  # }
  # w = {
  #   'numeric_label_path': '0.1.2.4.5.6'
  # }

  # # print lca(v,w)
  # x = {
  #   'depth': 1,
  #   'label_path': u'dblp.article',
  #   'numeric_label_path': u'0.6',
  #   'snode_id': 6L,
  #   'word': u'levy'
  #   }
  # y = {
  #   'depth': 2,
  #   'label_path': u'dblp.article.ee',
  #   'numeric_label_path': u'0.6.12',
  #   'snode_id': 12L,
  #   'word': u'levy'
  # }

  # lm =  lm(y, LIST['Levy'])
  # rm =  rm(y, LIST['Levy'])
  # print lm
  # print rm
  # print lca(rm, None)

  print lca('1.2.3.4.5.6', '')
