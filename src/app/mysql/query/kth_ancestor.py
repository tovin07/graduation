#!/usr/bin/python
# coding=utf-8

from xpath import xpath_evaluation
from settings import *

def kth_ancestor(unmarked):
  marked = {}
  results = {}
  # s: slca, r: results. s is unicode string, r is list
  while unmarked:
    for s, r in unmarked.items():
      # check if r is empty or not, if not empty move s, r from unmarked to marked
      if r:
        del unmarked[s]
        marked[s] = r
      else:
        sp = parent(s)
        # Remove s from S_umarked to prevent sp match its child s
        del unmarked[s]
        # If sp is not empty string, do it if you want
        if sp:
        # Check structural consistency of s in S_umarked and S_marked
          xu = [u for u in unmarked.keys() if sp in u]
          xm = [m for m in marked.keys() if sp in m]
          # If structural anomaly occurs in one of these two set => Next next next
          if xu or xm:
            unmarked[sp] = []
          else:
            r = xpath_evaluation([sp])
            marked[sp] = r[sp]
  for k, v in marked.items():
    if v:
      results[k] = v
  return results

def parent(snode):
  sp = '.'.join(snode.split('.')[:-1])
  return sp if sp else None


if __name__ == '__main__':
  from time import time
  from pprint import pprint
  unmarked = {
    u'0.6.9': [],
    u'0.6.8': [2194807, 2385864, 2924822, 2925112],
    u'0.46.52': [],
    u'0.36.39': [],
    u'0.36.38': [],
    u'0.6.114': [],
    u'0.46.48': [38781, 46319, 771368, 1275732, 1422850],
    u'0.87.89': [],
    u'0.79.81': [],
    u'0.1.3': [],
    u'0.87.92': []
  }
  results = kth_ancestor(unmarked)
  pprint(results)
