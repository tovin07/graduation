#!/usr/bin/python
# coding=utf-8

if __name__ == '__main__':
  from kth_ancestor import kth_ancestor
  from pprint import pprint
  from schema import get_posting_list
  from settings import *
  from slca import slca
  from time import time
  from xpath import xpath_evaluation
  from sys import argv

  # Get keyword and database
  if len(argv) == 1:
    keyword = KEYWORD
    db = SQL_DB
  elif len(argv) == 2:
    keyword = argv[1]
    db = SQL_DB
  else:
    keyword = argv[1]
    db = argv[2]

  TEST = [
    'parallel java inproceedings by',
    'caise hybrid müller journals sigops',
    'jsyml iscas',
    'fm icmcs',
    'fm ecsqaru',
    'birthday amast stacs',
    'pdpta icra focs ac bit',
    'slp set siamdm tapsoft',
    'acj yang icdt mst',
    'dke icc cikm'
  ]

  # Get SLCA list
  posting_lists = get_posting_list(keyword=TEST[3], db=db).values()
  slca = slca(posting_lists)

  # Get Query result for slca list above
  qr = xpath_evaluation(slca, keyword)

  # Generalize using kth_anscestor
  results = kth_ancestor(qr)

  pprint(results)
