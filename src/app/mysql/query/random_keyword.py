#!/usr/bin/python
# coding=utf-8

from MySQLdb import cursors
from pprint import pprint
from settings import *
from time import time
import MySQLdb
import random

def connect(db='xml_dblp'):
  return MySQLdb.connect(
    host=SQL_HOST,
    port=SQL_PORT,
    user=SQL_USER,
    passwd=SQL_PASSWORD,
    db=db,
    use_unicode=True,
    charset=SQL_CHARSET
  )

def close(connection):
  return connection.close()

def random_keyword(db='xml_dblp'):
  connection = connect(db)
  cursor = connection.cursor()

  stmt = '''
    select word
    from word
    order by count desc
    limit 1000
  '''

  cursor.execute(stmt)

  res = cursor.fetchall()

  words = [r[0] for r in res]
  # Close connection
  connection.close()
  return words


if __name__ == '__main__':
  length = 5
  db = 'xmark_02'
  words = random_keyword(db)
  keyword = []
  for x in xrange(0, 50):
    keyword.append(' '.join(random.sample(words, length)))

  for k in keyword:
    print k.encode('utf-8')
