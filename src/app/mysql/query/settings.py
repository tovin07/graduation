#!/usr/bin/python
# coding=utf-8

# MySQL configurations
SQL_HOST = 'localhost'
SQL_PORT = 3306
SQL_USER = 'root'
SQL_PASSWORD = '0000'
SQL_DB = 'xml_dblp'
SQL_CHARSET = 'utf8'

KEYWORD = 'quality analysis data'

# PUNCTUATION = '!"#$%&\'()*+,-./:;<=>?@[\\]^_`{|}~'
PUNCTUATION = '!"#$%&\'()*+,-./:;<=>?@[\\]^_`{|}~0123456789'
