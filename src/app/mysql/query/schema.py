#!/usr/bin/python
# coding=utf-8

from MySQLdb import connect, cursors
from pprint import pprint
from settings import *
from time import time
import string

def get_posting_list(keyword='XML Levy', db='xml_dblp'):
  connection = connect(
    host=SQL_HOST,
    port=SQL_PORT,
    user=SQL_USER,
    passwd=SQL_PASSWORD,
    db=db,
    use_unicode=True,
    charset=SQL_CHARSET
  )

  # Cursor
  cursor = connection.cursor()

  # Posting list
  L = {}

  stmt = '''
    select numeric_label_path
    from word_snode
    where word = %s
    order by snode_id
  '''

  # Split keyword
  replace_punctuation = string.maketrans(
    PUNCTUATION, ' '*len(PUNCTUATION))
  words = keyword.translate(replace_punctuation).lower().split()
  # print words

  # Fetch posting list
  for word in words:
    cursor.execute(stmt, (word,))
    res = cursor.fetchall()
    L[word] = [item[0] for item in res]

  # Remember to close connection
  cursor.close()
  connection.close()

  return L

def execute_statement(cursor, word):
  stmt = '''
    select word, snode_id, depth, label_path, numeric_label_path
    from word_snode
    where word = %s
    order by snode_id
  '''
  cursor.execute(stmt, word)
  schema_posting_list = cursor.fetchall()
  return {
    word: schema_posting_list
  }

if __name__ == '__main__':
  L = get_posting_list()
  pprint(L)
