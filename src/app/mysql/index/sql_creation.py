#!/usr/bin/python
# coding=utf-8

SQL_INIT_SCHEMA = [
  'DROP SCHEMA IF EXISTS %s;',
  'CREATE SCHEMA %s DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;',
  'USE %s;'
  ]

SQL_CREATE_TABLES = {
  'inode': '''
    CREATE TABLE `inode` (
      `inode_id` int(10) unsigned NOT NULL,
      `depth` tinyint(4) NOT NULL,
      `label` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
      `label_path` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
      `numeric_label_path` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
      `node_path` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
      `snode_id` int(10) unsigned DEFAULT NULL,
      `attributes` text COLLATE utf8_unicode_ci NOT NULL,
      `content` text COLLATE utf8_unicode_ci NOT NULL,
      PRIMARY KEY (`inode_id`),
      KEY `depth` (`depth`),
      KEY `label_path` (`label_path`),
      KEY `node_path` (`node_path`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
  ''',
  'label_path': '''
    CREATE TABLE `label_path` (
      `snode_id` int(10) unsigned NOT NULL,
      `depth` tinyint(3) unsigned NOT NULL,
      `label` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
      `label_path` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
      `numeric_label_path` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
      PRIMARY KEY (`snode_id`),
      UNIQUE KEY `label_path_UNIQUE` (`label_path`),
      KEY `depth` (`depth`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
  ''',
  'snode': '''
    CREATE TABLE `snode` (
      `snode_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
      `depth` tinyint(3) unsigned NOT NULL,
      `label` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
      `label_path` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
      `numeric_label_path` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
      PRIMARY KEY (`snode_id`),
      UNIQUE KEY `label_path_UNIQUE` (`label_path`),
      KEY `depth` (`depth`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
  ''',
  'word': '''
    CREATE TABLE `word` (
      `word_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
      `word` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
      `count` int(11) unsigned NOT NULL DEFAULT '1',
      PRIMARY KEY (`word_id`),
      UNIQUE KEY `word_UNIQUE` (`word`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
  ''',
  'word_inode': '''
    CREATE TABLE `word_inode` (
      `word` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
      `inode_id` int(10) unsigned NOT NULL,
      `snode_id` int(10) unsigned DEFAULT '0',
      `depth` tinyint(3) unsigned NOT NULL,
      `node_path` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
      `label_path` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
      `numeric_label_path` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
      PRIMARY KEY (`word`,`inode_id`),
      KEY `inode_id` (`inode_id`),
      KEY `snode_id` (`snode_id`),
      KEY `depth` (`depth`),
      KEY `label_path` (`label_path`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
  ''',
  'word_snode': '''
    CREATE TABLE `word_snode` (
      `word` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
      `snode_id` int(10) unsigned NOT NULL DEFAULT '0',
      `depth` tinyint(3) unsigned NOT NULL,
      `label_path` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
      `numeric_label_path` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
      PRIMARY KEY (`word`,`snode_id`),
      KEY `depth` (`depth`),
      KEY `snode_id` (`snode_id`),
      KEY `label_path` (`label_path`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
    '''
}

def create_database(connection, db='xml'):
  from settings import SQL_HOST, SQL_PORT, SQL_USER, SQL_PASSWORD, SQL_CHARSET
  import MySQLdb

  connection = connection or MySQLdb.connect(
    host=SQL_HOST,
    port=SQL_PORT,
    user=SQL_USER,
    passwd=SQL_PASSWORD,
    use_unicode=True,
    charset=SQL_CHARSET
  )

  # Get a cursor to access data from database
  cursor = connection.cursor()

  # Create schema
  for query in SQL_INIT_SCHEMA:
    statement = query % db
    cursor.execute(statement)

  # Create each table
  for name, query in SQL_CREATE_TABLES.items():
    cursor.execute(query)

  connection.commit()

  # Remember to close connection
  # connection.close()
  # This line for test only
  return connection

if __name__ == '__main__':
  # Open, create then close
  create_database().close()

