#!/usr/bin/python
# coding=utf-8

# MySQL configurations
SQL_HOST = 'localhost'
SQL_PORT = 3306
SQL_USER = 'root'
SQL_PASSWORD = '0000'
SQL_DB = 'xml'
SQL_CHARSET = 'utf8'

# MongoDB configuration
MONGO_URI = 'mongo://localhost:27017/xml'

SIZE = 5000

# XML Path to test
# XML_PATH = '/home/tovin07/Downloads/viwiki/xml/preorder.xml'
# XML_PATH = '/home/tovin07/Downloads/viwiki/xml/preorder_dup.xml'
# XML_PATH = '/home/tovin07/Downloads/viwiki/xml/fail.xml'
# XML_PATH = '/home/tovin07/Downloads/viwiki/xml/tiny.xml'
# XML_PATH = '/home/tovin07/Downloads/viwiki/xml/small.xml'
# XML_PATH = '/home/tovin07/Downloads/viwiki/xml/huge.xml'
# XML_PATH = '/home/tovin07/Downloads/dblp/hdblp.xml.120503'
# XML_PATH = '/home/tovin07/Downloads/dblp/dblp.xml'
# XML_PATH = '/home/tovin07/Downloads/dblp/dblp_entities_resolved.xml'
# XML_PATH = '/home/tovin07/Downloads/dblp/dblp_2002_entities_resolved.xml'
# XML_PATH = '/home/tovin07/Downloads/test/nasa.xml'
# XML_PATH = '/home/tovin07/Downloads/test/standard'
# XML_PATH = '/home/tovin07/Downloads/test/SigmodRecord.xml'
# XML_PATH = '/home/tovin07/Downloads/test/XMark_0.2.xml'
XML_PATH = '/home/tovin07/Downloads/test/XMark_1.0.xml'


# CSV Path
CSV_PATH = '/home/tovin07/Downloads/csv/dblp_2002/'


# PUNCTUATIONS
# punctuation = string.punctuation
# PUNCTUATION = '!"#$%&\'()*+,-./:;<=>?@[\\]^_`{|}~'
PUNCTUATION = '!"#$%&\'()*+,-./:;<=>?@[\\]^_`{|}~0123456789'
