#!/usr/bin/python
# coding=utf-8

class Node(object):
  """
  This class is used to store a general node in XML tree
  """
  def __init__(self, node_id, depth, label, label_path,
                node_path, word, attributes, content):
    self.node_id = node_id
    self.depth = depth
    self.label = label
    self.label_path = label_path
    self.node_path = node_path
    self.word = word
    self.attributes = attributes
    self.content = content


class SchemaNode(object):
  """
  Using to define a schema node in DataGuide+
  """
  def __init__(self, arg):
    super(SNode, self).__init__()
    self.arg = arg


class InstanceNode(object):
  """
  Using to define a instance node, except value nodes.
  """
  def __init__(self, arg):
    super(InstanceNode, self).__init__()
    self.arg = arg


class TestNode(object):
  """
  Generate test node for some function
  """

  # Sample data
  tag = 'bib'
  attrib = {
    'memory': 'in my short term memory',
    'sql': 'mysql is a database system',
    'string': 'can store some string',
    'plus': 'with a bunch of + - * / sign',
    'date': '29/05/2014',
    'number': 'my favorite numer is 7',
    'variable': 'test_variable',
    'unicode': u'Tất nhiên là phải thử với một số ký tự Unicode như bảng chữ cái tiếng Việt thì mới có thể đánh giá được xem việc loại bỏ những dấu câu không mong muốn mà vẫn thỏa mãn được tốc độ mà người lập trình hướng tới.',
    'link': 'http://www.vietnamnet.vn'
  }
  text = "Whao! A dot. A comma, an ellipsis... and a minus sign -"

  def __init__(self, tag='', attrib={}, text=''):
    self.tag = tag or self.tag
    self.attrib = attrib or self.attrib
    self.text = text or self.text


class TestRealNode(object):
  """
  Create a real node that coresponds to Node class
  """
  node_id = 0
  depth = 0
  label = 'bib'
  label_path = 'bib'
  node_path = '0'
  word = 'Tất nhiên là phải thử với một số ký tự Unicode như bảng chữ cái tiếng Việt thì mới có thể đánh 7 8 aa 1985 vietnamnet'.split()

  def __init__(self, node_id=0, depth=0, label='',
              label_path='', node_path='', word=[]):
    self.node_id = node_id or self.node_id
    self.depth = depth or self.depth
    self.label = label or self.label
    self.label_path = label_path or self.label_path
    self.node_path = node_path or self.node_path
    self.word = word or self.word


def create_data_node(inode_id, depth, label, label_path,
                    node_path, word, attributes, content):
  return  {
    'inode_id': inode_id,
    'snode_id': 0,
    'depth': depth,
    'label': label,
    'label_path': label_path,
    'numeric_label_path': '',
    'node_path': node_path,
    'word': word,
    'attributes': attributes,
    'content': content
  }

if __name__ == '__main__':
  from pprint import pprint
  node = TestNode()
  print 'tag:', node.tag
  print 'attributes', node.attrib
  print 'text:', node.text

