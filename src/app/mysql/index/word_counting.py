#!/usr/bin/python
# coding=utf-8

from node import *
from settings import *
from string_manipulation import get_words
from time import time
from pprint import pprint
import lxml.etree as etree
import MySQLdb
from itertools import chain

def iterparse(xmlPath=XML_PATH):
  # context to iterate
  context = etree.iterparse(
    source=xmlPath,
    events=('start', 'end'),
    remove_blank_text=True,
    encoding='utf-8',
    resolve_entities=True,
    huge_tree=True,
    recover=True)

  # Keep reference to root node, then we can clear memory after each loop
  event, root = context.next()
  # yield get_words(root)

  for event, node in context:
    if event == 'start':
      yield get_words(node)

    if event == 'end':
      # Clear reference to save memory, avoid memory leak
      node.clear()
      root.clear()

# Test functions in-place
if __name__ == '__main__':
  start = time()
  # SIZE = 10000
  #   # Create connection
  # connection = MySQLdb.connect(
  #     host=SQL_HOST,
  #     port=SQL_PORT,
  #     user=SQL_USER,
  #     passwd=SQL_PASSWORD,
  #     db=SQL_DB,
  #     use_unicode=True,
  #     charset=SQL_CHARSET
  #   )

  # # Get a cursor to access data from database
  # cursor = connection.cursor()

  # stmt = '''
  # insert ignore into word (word)
  # values (%s)
  # '''
  words = iterparse()

  # cursor.execute('truncate table word')
  # connection.commit()

  # count = 0
  # items = []
  # for word in words:
  #   items.extend(word)
  #   if len(items) > SIZE:
  #     res = cursor.executemany(stmt, items)
  #     count += res
  #     connection.commit()
  #     items = []
  #     print count, time() - start

  # res = cursor.executemany(stmt, items)
  # count += res
  # items = []
  # connection.commit()
  # connection.close()
  # print count, time() - start


  # count
  count = 0
  for word in words:
    for w in word:
      count += 1

  print count, time() - start

# 134 073 629 736.100888014 - dblp_2014.05.02 - remove number
# 15054033 43.0767660141 - dblp_2002 - not remove number
