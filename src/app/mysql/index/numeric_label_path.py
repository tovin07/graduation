#!/usr/bin/python
# coding=utf-8

import MySQLdb
from settings import *

def generate_numeric_label_path(connection=None, db='xml'):
  # Create connection
  connection = connection or MySQLdb.connect(
      host=SQL_HOST,
      port=SQL_PORT,
      user=SQL_USER,
      passwd=SQL_PASSWORD,
      db=db or SQL_DB,
      use_unicode=True,
      charset=SQL_CHARSET
    )

  # Get a cursor to access data from database
  cursor = connection.cursor()

  # Statement for query
  stmt = '''
    select label_path, snode_id
    from label_path
  '''
  cursor.execute(stmt)

  # fetch data from query
  data = dict(cursor.fetchall())    # Note, this works with tuple 2 only!
  id_numeric = scan_over_fetched_data(data)

  update_stmt = '''
    update label_path
    set numeric_label_path = %s
    where snode_id = %s
  '''

  cursor.executemany(update_stmt, id_numeric)

  connection.commit()

  # return id_numeric
  return ['just', 'some', 'test', 'values', 'here']

# Scan over dictionary of data to build up numeric_label_path
def scan_over_fetched_data(data):
  # List of label_path
  paths = data.keys()

  # Loop over each label_path and search sub_label_path
  for path in paths:
    labels = path.split('.')  # Split path into pieces
    tmp = []                  # Hold list of label to make sub_label
    numeric = []              # Hold list of numeric label
    # Loop over each label to find out some interesting things :v
    for label in labels:
      tmp.append(label)                       # Append node after each loop
      label_path = '.'.join(tmp)              # Create sub_label_path
      numeric.append(str(data[label_path]))   # Numeric list

    # Yield (snode_id, numeric_label_path) after each big loop
    yield ('.'.join(numeric), numeric[-1])


# test in-place
if __name__ == '__main__':
  id_numeric = generate_numeric_label_path()
  for item in id_numeric:
    print item
