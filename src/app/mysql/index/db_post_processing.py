#!/usr/bin/python
# coding=utf-8

def create_dataguide(connection, cursor):
  commands = [
    '''
      insert ignore into snode (depth, label, label_path)
        select depth, label, label_path
        from inode
        order by inode_id asc;
    ''',
    '''
      create temporary table if not exists t1
        as (select (snode_id - 1) as id, depth, label, label_path from snode);
    ''',
    '''
      insert ignore into label_path (snode_id, depth, label, label_path)
        select id, depth, label, label_path from t1;
    ''',
    '''
      drop temporary table t1;
    '''
  ]

  count = 0
  for command in commands:
    count += 1
    print '[CREATE_DATAGUIDE] Run query', count
    cursor.execute(command)

  connection.commit()


def update_misc_things(connection, cursor):
  commands = [
    # Update snode_id in inode table ~ 10 minutes
    '''
      update inode a
      inner join label_path b
      on a.label_path = b.label_path
      set a.numeric_label_path = b.numeric_label_path, a.snode_id = b.snode_id;
    ''',
    # Update table word_inode | aka. instance-index  | at least 30 minute
    '''
      update word_inode a
        inner join label_path b
        on a.label_path = b.label_path
        set a.numeric_label_path = b.numeric_label_path, a.snode_id = b.snode_id;
    ''',

    # Create word_snode table | aka. schema-index | hours
    '''
      insert ignore into word_snode (word, snode_id, depth, label_path, numeric_label_path)
        select word, snode_id, depth, label_path, numeric_label_path
        from word_inode;
    '''
  ]

  count = 0
  for command in commands:
    count += 1
    print '[UPDATE_MISC_THINGS] Run query', count
    cursor.execute(command)

  connection.commit()
