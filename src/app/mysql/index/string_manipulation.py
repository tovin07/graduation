#!/usr/bin/python
# coding=utf-8

from settings import PUNCTUATION
from time import time
import string

def get_text(node):
  # Use lowercase only
  label = node.tag
  attr = [k + ' ' + v for (k,v) in node.attrib.items()]
  content = node.text if node.text else u''

  # Join all text in to one
  text = ' '.join([label, content] + attr).encode('utf-8').lower()
  # text = ' '.join([label, content] + attr).encode('utf-8').lower()

  # Remove punctuation, can be used for unicode
  replace_punctuation = string.maketrans(
    PUNCTUATION, ' '*len(PUNCTUATION))
  word = text.translate(replace_punctuation)

  # Ensure to remove extra-space and then remove duplicate elements
  word = frozenset([(w,) for w in word.split()])
  # Test in file
  # word = frozenset(word.decode('utf-8').split())

  # return set of (word,) tuples not string separated by space
  return {
    'label': label,
    'attributes': str(node.attrib),
    'content': content,
    'word': word
  }


################################## TEST AREA ###################################

# Generate Nodes
def gen_nodes(size):
  for x in xrange(0, size):
    yield TestNode()

def create_real_nodes(nodes):
  from node import Node
  from random import randint, choice
  from snode import TAGS_LISTS

  depth = 0
  node_id = 0
  incomming_labels = ['xxx', 'asdashj']
  incomming_ids = ['123', '1231231']
  for node in nodes:
    depth += 1
    node_id += 1
    incomming_labels.append(choice(TAGS_LISTS))
    incomming_ids.append(str(choice(xrange(0, randint(1, 100000)))))
    label_path = '.'.join(incomming_labels) # Create label_path
    node_path = '.'.join(incomming_ids)     # Create node_path

    text = get_text(node)                   # Create list of unique words
    word = text['word']
    attributes = text['attributes']
    content = text['content']
    # yield each node for db processing
    yield Node(node_id, depth, node.tag, label_path, node_path,
              word, attributes, content)

    depth -= 1
    incomming_labels.pop()
    incomming_ids.pop()

def create_list_of_texts(nodes):
  for node in nodes:
    yield get_text(node)

# Test above function(s)
if __name__ == '__main__':
  from node import TestNode
  from pprint import pprint
  from db_processing import bulk_insert

  size = 10000
  nodes = gen_nodes(size)

  real_nodes = create_real_nodes(nodes)

  bulk_insert(db='xxx', nodes=real_nodes, size=1000)
