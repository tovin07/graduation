#!/usr/bin/python
# coding=utf-8

from db_post_processing import *
from iterparse import iterparse
from numeric_label_path import generate_numeric_label_path
from pprint import pprint
from settings import *
from sql_creation import create_database
from time import time
import MySQLdb

def bulk_insert(db=SQL_DB, nodes=[], size=SIZE):
  # Create connection
  connection = MySQLdb.connect(
      host=SQL_HOST,
      port=SQL_PORT,
      user=SQL_USER,
      passwd=SQL_PASSWORD,
      use_unicode=True,
      charset=SQL_CHARSET
  )

  # Get a cursor to access data from database
  cursor = connection.cursor()

  # Create database to wrk in
  create_database(connection, db)
  cursor.execute('set autocommit=0')
  connection.commit()

  # get xml data generator object from lxml.etree.iterparse
  nodes = nodes or iterparse()

  # Create statements and items to insert
  statements = get_statements()
  items = get_items(nodes, size)

  count = {}  # Count number of node, word that successful inserted
  for k in statements.keys():
    count[k] = 0

  for item in items:
    start = time()
    for k in statements.keys():
      res = cursor.executemany(statements[k], item[k]) or 0
      count[k] += res
    print round(time() - start, 2)
    pprint(count)
    print '-'*80
    connection.commit()

  cursor.execute('set autocommit=1')
  connection.commit()

  print 'READ XML DONE!'
  print '='*80
  # Create dataguide
  create_dataguide(connection, cursor)
  print '[CREATE_DATAGUIDE] Create DataGuide+ done!'

  # Generate numeric_label_path
  generate_numeric_label_path(connection, db)
  print '[GENERATE_NUMERIC_LABEL_PATH] Generate done!'

  # Update misc things
  update_misc_things(connection, cursor)
  print '[UPDATE_MISC_THINGS] Done all!'
  # remember to close connection
  connection.close()

def get_statements():
  inode = '''
    insert ignore into
      inode (inode_id, depth, label, label_path, node_path, attributes, content)
    values (%s, %s, %s, %s, %s, %s, %s)
  '''
  word_inode = '''
    insert ignore into
      word_inode (word, inode_id, depth, node_path, label_path)
    values (%s, %s, %s, %s, %s)
  '''
  word = '''
    insert into
      word (word)
    values (%s)
    on duplicate key update count = count +1
  '''

  return {
    'inode': inode,
    'word_inode': word_inode,
    'word': word
  }

def get_items(nodes, size):
  # initialize data to insert
  inode = []
  word_inode = []
  word = []

  # Loop over nodes and yield after each size reached
  for n in nodes:
    inode.append((n.node_id, n.depth, n.label, n.label_path,
                  n.node_path, n.attributes, n.content))
    for w in n.word:
      word_inode.append((w, n.node_id, n.depth, n.node_path, n.label_path))

    word.extend(n.word)

    if n.node_id % size == 0:
      yield {
        'inode': inode,
        'word_inode': word_inode,
        'word': word
      }
      # Empty current data
      inode = []
      word_inode = []
      word = []
    # yield to ensure all data is writed
    # This happens when nodes reach the end of it
  yield {
    'inode': inode,
    'word_inode': word_inode,
    'word': word
  }

# File

def bulk_write(nodes=[], dir_path=CSV_PATH, size=SIZE):
  import codecs
  nodes = nodes or iterparse()
  lines = get_lines(nodes, size)
  file_names = ['inode', 'word', 'word_inode']
  file_paths = [(CSV_PATH + name) for name in file_names]

  file_dicts = dict(zip(file_names, file_paths))

  files = {}
  for name, path in file_dicts.items():
    files[name] = codecs.open(path, 'wb+', 'utf-8')

  for line in lines:
    for k, f in files.items():
      f.write(line[k])

  for f in files.values():
    f.close()

def get_lines(nodes, size):
  # initialize data to insert
  inode = []
  word_inode = []
  word = []

  # Loop over nodes and yield after each size reached
  for n in nodes:
    inode.append('++'.join([str(n.node_id), str(n.depth),
                            n.label, n.label_path,
                            n.node_path, n.attributes, n.content]))
    for w in n.word:
      word_inode.append('++'.join([w, str(n.node_id),
                        str(n.depth), n.node_path, n.label_path]))

    word.extend(n.word)

    if (n.node_id+1) % size == 0:
      yield {
        'inode': '\n'.join(inode),
        'word_inode': '\n'.join(word_inode),
        'word': '\n'.join(word)
      }
      # Empty current data
      inode = []
      word_inode = []
      word = []
    # yield to ensure all data is writed
    # This happens when nodes reach the end of it
  yield {
    'inode': '\n'.join(inode),
    'word_inode': '\n'.join(word_inode),
    'word': '\n'.join(word)
  }

# Test in-place
if __name__ == '__main__':
  bulk_insert()
