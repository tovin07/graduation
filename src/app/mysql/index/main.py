#!/usr/bin/python
# coding=utf-8

if __name__ == '__main__':
  from db_processing import bulk_write, bulk_insert
  from node import TestRealNode
  from settings import SIZE
  from sys import argv
  from time import time

  start = time()

  print 'Please be patient, it will take more than 10 minutes'
  print 'Each %d nodes, one information line will be printed' % SIZE

  db = argv[1] if len(argv) > 1 else 'xml_dblp'
  bulk_insert(db=db, size=SIZE)
  # bulk_write()

  print 'Done in:', time() - start, 's'
