#!/usr/bin/python
# coding=utf-8

from configs import MONGO_URI, MONGO_DB, INDEXES, SIZE
from pymongo import MongoClient

def insert(data, database='xml_test', size=SIZE):
  # Mongo Instance
  client = MongoClient()

  # Drop existing database
  client.drop_database(database)

  # Get cursor to new database
  db = client[database]

  # Create index before insert
  index(db, INDEXES)

  for d in data:
    items, count = [], 0
    for item in d['data']:
      count += 1
      items.append(item)
      if count == size:
        db[d['collection']].insert(items)
        items, count = [], 0

    db[d['collection']].insert(items)

  # Remember to disconect server
  client.close()

def index(db, indexes=INDEXES):
  for index in indexes:
    collection = index['collection']
    for i in index['info']:
      db[collection].ensure_index([(i['field'], i['order'])],
                                  unique=i['unique'])

def create_items(data):
  inode = {'collection': 'inode', 'data': data}

  return [inode,]
  pass

# Test functions in-place
if __name__ == '__main__':
  from node import create_data_node, TestDataNode
  data_nodes = [create_data_node(TestDataNode())]
  insert([{'collection': 'inode', 'data': data_nodes}])
