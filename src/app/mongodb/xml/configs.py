#!/usr/bin/python
# coding=utf-8

MONGO_URI = 'mongodb://localhost:27017/'
MONGO_DB = 'xml'
SIZE = 5000                               # Insert size

# XML Path to test
XML_PATH = '/home/tovin07/Downloads/test/preorder.xml'
# XML_PATH = '/home/tovin07/Downloads/viwiki/xml/preorder_dup.xml'
# XML_PATH = '/home/tovin07/Downloads/viwiki/xml/fail.xml'
# XML_PATH = '/home/tovin07/Downloads/viwiki/xml/tiny.xml'
# XML_PATH = '/home/tovin07/Downloads/viwiki/xml/small.xml'
# XML_PATH = '/home/tovin07/Downloads/viwiki/xml/huge.xml'
# XML_PATH = '/home/tovin07/Downloads/dblp/hdblp.xml.120503'
# XML_PATH = '/home/tovin07/Downloads/dblp/dblp.xml'
# XML_PATH = '/home/tovin07/Downloads/dblp/dblp_entities_resolved.xml'
# XML_PATH = '/home/tovin07/Downloads/dblp/dblp_2002_entities_resolved.xml'
# XML_PATH = '/home/tovin07/Downloads/test/nasa.xml'
# XML_PATH = '/home/tovin07/Downloads/test/standard'
# XML_PATH = '/home/tovin07/Downloads/test/SigmodRecord.xml'


# PUNCTUATIONS
# punctuation = string.punctuation
# PUNCTUATION = '!"#$%&\'()*+,-./:;<=>?@[\\]^_`{|}~'
PUNCTUATION = '!"#$%&\'()*+,-./:;<=>?@[\\]^_`{|}~0123456789'


# INDEX INFO
from pymongo import ASCENDING, DESCENDING
INDEXES = [
  {
    'collection': 'inode',
    'info': [
      {
        'field': 'inode_id',
        'order': ASCENDING,
        'unique': False
      },
      {
        'field': 'depth',
        'order': ASCENDING,
        'unique': False
      },
      {
        'field': 'label_path',
        'order': ASCENDING,
        'unique': False
      },
    ]
  },
  {
    'collection': 'snode',
    'info': [
      {
        'field': 'snode_id',
        'order': ASCENDING,
        'unique': False
      },
      {
        'field': 'depth',
        'order': ASCENDING,
        'unique': False
      },
      {
        'field': 'label_path',
        'order': ASCENDING,
        'unique': False
      },
    ]
  },
  {
    'collection': 'word_inode',
    'info': [
      {
        'field': 'word',
        'order': ASCENDING,
        'unique': False
      },
      {
        'field': 'inode_id',
        'order': ASCENDING,
        'unique': False
      },
      {
        'field': 'snode_id',
        'order': ASCENDING,
        'unique': False
      },
      {
        'field': 'depth',
        'order': ASCENDING,
        'unique': False
      },
      {
        'field': 'label_path',
        'order': ASCENDING,
        'unique': False
      },
    ]
  },
  {
    'collection': 'word_snode',
    'info': [
      {
        'field': 'word',
        'order': ASCENDING,
        'unique': False
      },
      {
        'field': 'snode_id',
        'order': ASCENDING,
        'unique': False
      },
      {
        'field': 'depth',
        'order': ASCENDING,
        'unique': False
      },
      {
        'field': 'label_path',
        'order': ASCENDING,
        'unique': False
      },
    ]
  },
  {
    'collection': 'word',
    'info': [
      {
        'field': 'word',
        'order': ASCENDING,
        'unique': True
      }
    ]
  }
]
