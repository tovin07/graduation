#!/usr/bin/python
# coding=utf-8

from configs import PUNCTUATION
import string

def get_text(node):
  # Use lowercase only
  label = node.tag.encode('utf-8')
  attr = [(k + ': ' + v).encode('utf-8') for (k,v) in node.attrib.items()]
  content = node.text if node.text else u''

  # Join all text in to one
  text = ' '.join([label, content] + attr).encode('utf-8').lower()

  # Remove punctuation, can be used for unicode
  replace_punctuation = string.maketrans(
    PUNCTUATION, ' '*len(PUNCTUATION))
  word = text.translate(replace_punctuation)

  # Ensure to remove extra-space and then remove duplicate elements
  word = list(frozenset(word.split()))

  # return set of (word,) tuples not string separated by space
  return {
    'label': label,
    'attributes': ' | '.join(attr),
    'content': content,
    'word': word
  }


# Test functions
if __name__ == '__main__':
  from node import XMLNode

  data = get_text(XMLNode())
  print data['label']
  print data['attributes']
  print data['content']
  for w in data['word']:
    print w
