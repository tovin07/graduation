#!/usr/bin/python
# coding=utf-8

from node import DataNode
from configs import *
from strproc import get_text
from time import time
from pprint import pprint
import lxml.etree as etree

def iterparse(xmlPath=XML_PATH):
  # context to iterate
  context = etree.iterparse(
    source=xmlPath,
    events=('start', 'end'),
    remove_blank_text=True,
    encoding='utf-8',
    resolve_entities=True,
    huge_tree=True,
    recover=True)

  # Keep reference to root node, then we can clear memory after each loop
  event, root = context.next()

  # Initialize depth, node_id, incomming_labels, incomming_ids
  node_id = 0
  depth = 0
  incomming_labels = [root.tag]
  incomming_ids = [str(node_id)]  # Keep it all-string for easy to join

  # Root node
  root_text = get_text(root)
  data_node =  create_data_node(DataNode(node_id, depth, root.tag,
    '.'.join(incomming_labels), '.'.join(incomming_ids),
    root_text['word'], root_text['attributes'], root_text['content']))

  yield data_node
  # Initialize tags list with root label
  tags = set([root.tag])

  for event, node in context:
    if event == 'start':
      if '.' not in node.tag:
        tags.add(node.tag)
        depth += 1
        node_id += 1
        incomming_labels.append(node.tag)       # Append current node label
        incomming_ids.append(str(node_id))      # Append current id
        label_path = '.'.join(incomming_labels) # Create label_path
        node_path = '.'.join(incomming_ids)     # Create node_path

        text = get_text(node)                   # Create list of unique words
        word = text['word']
        attributes = text['attributes']
        content = text['content']
        # yield each node for db processing
        yield DataNode(node_id, depth, node.tag, label_path, node_path,
                  word, attributes, content)
      else:
        continue

    if event == 'end':
      # Decrease depth
      depth -= 1
      # Remove last item in each list
      if '.' not in node.tag:
        incomming_labels.pop()
        incomming_ids.pop()
      else:
        continue
      # Clear reference to save memory, avoid memory leak
      node.clear()
      root.clear()

  # print tags


def print_node(node):
  print 'ID:', node.inode_id
  print 'Depth:', node.depth
  print 'Label:', node.label
  print 'Label path:', node.label_path
  print 'Node path', node.node_path
  print 'Key words:'
  for word in node.word:
    print word
  print 'Attributes:', node.attributes
  print 'Content:', node.content.encode('utf-8')
  print '-'*80


# Test functions in-place
if __name__ == '__main__':
  xml_tree_data = iterparse()
  for node in xml_tree_data:
    # if node.node_id % 100000 == 0:
      print_node(node)
