#!/usr/bin/python
# coding=utf-8

if __name__ == '__main__':
  from configs import XML_PATH
  from dbproc import insert
  from parsing import iterparse
  from sys import argv
  from time import time

  start = time()
  print 'Please be patient, it will take more than 10 minutes'
  print 'Each 5000 nodes, one information line will be printed'

  db = argv[1] if len(argv) > 1 else 'xml_test'
  insert(data=iterparse(XML_PATH), database=db, size=5000)

  # bulk_insert(db=db, data_nodes=data_nodes, bulk_size=bulk_size)

  print '='*80
  print '[INFO] EVERYTHING DONE IN:', round(time() - start, 2), '(s)'
