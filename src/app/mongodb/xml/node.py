#!/usr/bin/python
# coding=utf-8

class Node(object):
  """
  General Node
  """
  def __init__(self, name='test'):
    self.name = name


class XMLNode(object):
  """
  docstring for XMLNode
  """
  tag = 'root'
  attrib = {
    'memory': 'in my short term memory',
    'sql': 'mysql is a database system',
    'string': 'can store some string',
    'plus': 'with a bunch of + - * / sign',
    'date': '29/05/2014',
    'number': 'my favorite numer is 7',
    'variable': 'test_variable',
    'unicode': u'Tất nhiên là phải thử với một số ký tự Unicode như bảng chữ cái tiếng Việt thì mới có thể đánh giá được xem việc loại bỏ những dấu câu không mong muốn mà vẫn thỏa mãn được tốc độ mà người lập trình hướng tới.',
    'link': 'http://www.vietnamnet.vn'
  }
  text = "Whao! A dot. A comma, an ellipsis... and a minus sign -, tiếng Việt thì mới có thể đánh giá"

  def __init__(self, tag='', attrib='', text=''):
    self.tag = tag or self.tag
    self.attrib = attrib or self.attrib
    self.text = text or self.text


class DataNode(object):
  """
  docstring for DataNode
  """
  def __init__(self, inode_id, depth, label, label_path,
                node_path, word, attributes, content):
    self.inode_id = inode_id
    self.snode_id = 0
    self.depth = depth
    self.label = label
    self.label_path = label_path
    self.numeric_label_path = ''
    self.node_path = node_path
    self.word = word
    self.attributes = attributes
    self.content = content


class TestDataNode(object):
  """
  docstring for TestDataNode
  """
  inode_id = 0
  depth = 0
  label = 'root'
  label_path = 'root'
  node_path = '0'

  def __init__(self, inode_id=0, depth=0, label='', label_path='',
                node_path='', word='', attributes='', content=''):
    self.inode_id = inode_id or self.inode_id
    self.snode_id = 0
    self.depth = depth or self.depth
    self.label = label or self.label
    self.label_path = label_path or self.label_path
    self.numeric_label_path = ''
    self.node_path = node_path or self.node_path

    if word:
      self.word = word
      self.attributes = attributes
      self.content = content
    else:
      data = self.get_sample_data()
      self.word = data['word']
      self.attributes = data['attributes']
      self.content = data['content']

  def get_sample_data(self):
    from strproc import get_text
    return get_text(XMLNode())


############################## FUNCTONS ########################################

def create_data_node(inode_id, depth, label, label_path,
                    node_path, word, attributes, content):
  return  {
    'inode_id': inode_id,
    'snode_id': 0,
    'depth': depth,
    'label': label,
    'label_path': label_path,
    'numeric_label_path': '',
    'node_path': node_path,
    'word': word,
    'attributes': attributes,
    'content': content
  }

if __name__ == '__main__':
  data_node = TestDataNode()
  print data_node.attributes
  print data_node.content
  for w in data_node.word:
    print w
